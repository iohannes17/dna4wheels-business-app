import React, { Component } from 'react';
import './App.css';
import NavbarPage from '../src/Component/Nav/Nav'
import LandingPage from '../src/Component/Landing/LandingPage'
import About from '../src/Component/About/About'
import ContactPage from '../src/Component/Contact/contact';
import Pricing from '../src/Component/Pricing/pricing'
import Service from '../src/Component/Services/services'
import DashBoard from '../src/Component/DashBoard/DashBoard';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {HashRouter,Route} from 'react-router-dom'; // react-router v4/v5
import { ConnectedRouter } from 'connected-react-router'
// import { history } from './configurestore'
import Register from '../src/Component/Register/register'
import ForgotPassword from '../src/Component/ForgotPassWord/fpassword'
import {history} from './history'



class RootApp extends Component {
  render() {
    return (

      <ConnectedRouter history={history}> 



      <HashRouter>
      {this.props.isLoggedIn ?       
            <Route exact path="/" component={DashBoard}/>  
            :
            <div>
              <NavbarPage/> 
              <Route exact path="/" component={LandingPage} />
              <Route exact path="/home" component={LandingPage} />
              <Route path="/about" component={About}/>
              <Route path="/contact" component={ContactPage}/>
              <Route path="/pricing" component={Pricing}/>
              <Route path="/service" component={Service}/> 
              <Route path="/register" component={Register}/> 
              <Route path="/fpassword" component={ForgotPassword}/>
           </div>
      }
      
 
      </HashRouter>
    </ConnectedRouter>

    );
  }
}

    
const mapStateToProps = state =>{
  return{
      isLoggedIn: state.isUserLoggedIn.isLoggedIn,
      
  };
}

RootApp.propTypes ={
  isLoggedIn:PropTypes.bool.isRequired,
}



export default connect(mapStateToProps, {}) (RootApp);