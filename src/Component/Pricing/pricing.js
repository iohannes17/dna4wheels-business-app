import React, { Component } from 'react';
import './pricing.css';


export default class Pricing extends Component {


      
  render() {



     let renderBoard = <div className="row"> 
         
           
         <div className="col-md-4 col-sm-4 col-lg-4 col-xl-4">
              <div className="card pricecard">
                <div className="card-body">
                <center>
                 
                   <p id="priceheader"> Less than 1000 Report</p>
                     <p className="grey-text">N4200 for 1 report</p>

                     <div className="priceline"></div> 

                     <p className="grey-text">1000 Dna4Wheels report for 1000 cars</p>

                     <div className="priceline"></div> 
                     <p>Each Report will be available in your account for 30 days</p>

                     <div className="priceline"></div> 

                      <p><strong>Check for 1000 different cars</strong></p>

                      <div className="priceline"></div> 

                       <p><strong>Pack is valid for 1 year</strong></p>


                      <div className="modal-footer grey lighten-2 white-text">
                        <br/><br/><br/></div> </center>    
           </div>
              </div>       
           </div>

              <div className="col-md-4 col-sm-4 col-lg-4 col-xl-4">
              <div className="card pricecard">
                <div className="card-body">
                <center>
               
                   <p id="priceheader"> 1001-2000 Report</p>
                     <p className="grey-text">N3,675 per report</p>

                     <div className="priceline"></div> 

                     <p className="grey-text">1000+ Dna4Wheels report for 1000+ cars</p>

                     <div className="priceline"></div> 
                     <p>Each Report will be available in your account for 30 days</p>

                     <div className="priceline"></div> 

                      <p><strong>Check for 1000+ different cars</strong></p>

                      <div className="priceline"></div> 

                       <p><strong>Pack is valid for 1 year</strong></p>


                      <div className="modal-footer grey lighten-2 white-text">
                        <br/><br/><br/></div> </center>    
           </div>
              </div>       
           </div>

            <div className="col-md-4 col-sm-4 col-lg-4 col-xl-4">
              <div className="card pricecard">
                <div className="card-body">
                <center>
                   <p id="priceheader"> above 2001 Report</p>
                     <p className="grey-text">N3,150 per report</p>

                     <div className="priceline"></div> 

                     <p className="grey-text">2001+ Dna4Wheels report for different cars</p>

                     <div className="priceline"></div> 
                     <p>Each Report will be available in your account for 30 days</p>

                     <div className="priceline"></div> 

                      <p><strong>Check for 2001+ different cars</strong></p>

                      <div className="priceline"></div> 

                       <p><strong>Pack is valid for 1 year</strong></p>


                      <div className="modal-footer grey lighten-2 white-text">
                        <br/><br/><br/></div> </center>    
           </div>
              </div>       
           </div>

           
            
          </div>;
     
    return (

      <div>
          <div className="row">

        

          <div className="col-md-10 offset-md-1">
          <center>
              <h2 id="packageHeader">Dna4Wheels Vehicle History Report Pricing </h2>
         <p className="text-txt">
              Dna4Wheels is the most trusted source of information in Europe about the vehicle history of U.S. cars. 
              Thanks to the Dna4Wheels Vehicle History Report, it is possible to check the history of American import cars before you buy.</p> 
              <p className="text-txt">Take the advantage of the VIN number check and find out if the used car you're about to buy has a negative history record reported to Dna4Wheels.
              </p>
              <h2 id="priceheader">VIEW PRICING PLANS </h2>
          </center>
         </div>

         
       </div>

       <hr/>


        <div className="container" >
        <div className="row">
        <div className="col-md-0 col-sm-0 col-lg-0 col-xl-0">
         </div>
         <div className="col-md-12 col-sm-12 col-lg-12 col-xl-12">
       
                      {renderBoard}
                     
    

         </div>
         <div className="col-md-0 col-sm-0 col-lg-0 col-xl-0">
         </div>
        </div>  
      </div>

</div>     
    )
  }
}
