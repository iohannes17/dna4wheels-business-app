import React, { Component } from 'react';
import './contact.css';

class ContactPage extends Component {
  render() {
    return(
      <div className="container">
       
        <div id="contactlayout">
          <h2 className="h1-responsive font-weight-bold text-center my-5" id="priceheader">Contact us</h2>
          <p className="text-center w-responsive mx-auto pb-5">DNA4wheels is always available and is willing to answer any form of complaint or feedbacks
           in serving you better. We alawys appreciate your opinions and feedsbacks.</p>
          <div className="row">

            <div className="col-6">
              <div className="card">
                <div className="card-body">
                <div className="login one-edge-shadow">
                                <div className="login-triangle"></div>
                                 <h2 className="login-header">Message Us</h2>

                                <form className="login-container">
                                  <p><input type="email" placeholder="Email"/></p>
                                  <p><textarea id="form7" class="md-textarea form-control" rows="5"></textarea></p>
                                  <p><input type="submit" value="Submit"/></p>
                                </form>
                              </div>
                </div>
              </div>
            </div>
            <div className="col-6">
              <div className="map-responsive">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3964.105528075078!2d3.3699719147705838!3d6.508324595292197!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x103b8c5b8754da57%3A0x114c3780ce4ca42b!2s270+Murtala+Muhammed+Way%2C+Sabo+yaba%2C+Lagos!5e0!3m2!1sen!2sng!4v1535311290108" width="650" height="400" frameborder="0" style={{border:'0'}} allowfullscreen title="carfacts-map"></iframe>
              </div>
              <br/>
              <div className=" row text-center">
                <div className="col-md-4">
               
                <i class="fa fa-map-marker" style={{fontSize:30,color:'red'}}></i>
                  
                  <p>270,Muritala-Mohammed Yaba</p>
                  <p className="mb-md-0">Lagos, Nigeria</p>
                </div>
                <div className="col-md-4">
                  
                <i class="fa fa-phone"></i>
               
                  <p>+ 234 700 0CA RFAX</p>
                  <p className="mb-md-0">Mon - Fri 9:00am-5:00pm</p>
                </div>
                <div className="col-md-4">
                  
                <i class="fa fa-envelope"></i>
               
                  <p>info@carfacts.ng</p>
                  <p className="mb-md-0">oalofoje@carfacts.ng</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };
}

export default ContactPage;