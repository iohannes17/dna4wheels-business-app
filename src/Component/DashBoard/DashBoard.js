import React, { Component } from 'react'
import './DashBoard.css';
import Overview from '../DashBoard/SideNav/overview/overview'
import VinCheck from '../DashBoard/SideNav/VinCheck/vincheck'
import PasswordChange  from '../DashBoard/SideNav/ChangePassword/changePass'
import Profile from '../DashBoard/SideNav/userProfile/profile'
import VinHistory from '../DashBoard/SideNav/ViinHistory/history'
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {UserLogout} from '../../Actions/logOutAction';
import {FetchUserInfo} from '../../Actions/userInfoActions';
import Notifications from '../DashBoard/SideNav/Notification/notification'
import {withRouter,Redirect} from 'react-router'







class DashBoard extends Component {


  constructor(props){
    super(props)
    this.state ={
       toggle:'',
       active:'is-active',
       homebg:'#fec005',
       historybg:'transparent',
       profilebg:'transparent',
       passwordbg:'transparent',
       searchbg:'transparent',
       notification:'transparent'
    }
    this.handleLogout = this.handleLogout.bind(this);
  }


  componentDidMount(){
    this.props.FetchUserInfo(this.props.token);
   
  }


  onToggleSearch = ()=>{
          
    this.setState({
      toggle: 'search',
      search:'#fec005',
      homebg:'transparent',
      historybg:'',
      profilebg:'',
      passwordbg:'',
      notification:'transparent'

    });

  }


  onToggleHome = ()=>{
          
    this.setState({
      toggle: 'home',
      search:'transparent',
      homebg:'#fec005',
      historybg:'transparent',
      profilebg:'transparent',
      passwordbg:'transparent',
      notification:'transparent'

    });

  }


  onToggleHistory = ()=>{
          
    this.setState({
      toggle: 'history',
      search:'#fec005',
      homebg:'transparent',
      historybg:'transparent',
      profilebg:'transparent',
      passwordbg:'transparent',
      notification:'transparent'

    });

  }

  onToggleProfile = ()=>{
          
    this.setState({
      toggle: 'profile',
      search:'transparent',
      homebg:'transparent',
      historybg:'transparent',
      profilebg:'#fec005',
      passwordbg:'transparent',
      notification:'transparent'
    });

  }


  onTogglePassword = ()=>{
          
    this.setState({
      toggle: 'Password change',
      search:'transparent',
      homebg:'transparent',
      historybg:'transparent',
      profilebg:'transparent',
      passwordbg:'#fec005',
      notification:'transparent'
    });

  }



  onToggleNotification= ()=>{
          
    this.setState({
      toggle: 'notification',
      search:'transparent',
      homebg:'transparent',
      historybg:'transparent',
      profilebg:'transparent',
      passwordbg:'transparent',
      notification:'#fec005'
    });

  }

  handleLogout(e){
  

     e.preventDefault()  
     this.props.UserLogout();
    
     }






  render() {

     const toggle = this.state.toggle;
     let content =  <div>
                      <h1 className="page-title">Overview</h1>
                      <Overview/>
                      </div>;
    

    if(toggle === 'search'){
      content = <div>
                <h1 className="page-title">Vehicle History Report</h1>
                <VinCheck/>
                </div>;
    }

    else if(toggle === 'home'){
     content =  <div>
      <h1 className="page-title">Overview</h1>
      <Overview/>
      </div>;

    }
      

    else if(toggle === 'history'){
      content =  <div>
       <h1 className="page-title">Recent Vin History</h1>
       <VinHistory/>
       </div>;
 
     }
      
     
    else if(toggle === 'profile'){
      content =  <div>
       
       <Profile/>
       </div>;
 
     }

     else if(toggle === 'notification'){
      content =  <div>
         <h1 className="page-title">Notification</h1>
       <Notifications/>
       </div>;
 
     }


    else if(toggle === 'Password change'){
      content =  <div>
       <h1 className="page-title">Change Password</h1>
       <PasswordChange/>
       </div>;
 
     }
       
       
  
     const username = this.props.user.userInfoList.username;
    


    return (
        <div>  
      <div className="sidebar-is-reduced body-in-dash">
        <header className="l-header">
          <div className="l-header__inner clearfix">
          
            
      <div className="c-header-icon has-dropdown" style={{backgroundColor:this.state.notification}} onClick={this.onToggleNotification}>
     
      <i className="fa fa-bell" ></i>
        <div className="c-dropdown c-dropdown--notifications">
          <div className="c-dropdown__header"></div>
          <div className="c-dropdown__content"></div>
        </div>
      </div>
     
      <div className="header-icons-group">
       <div className="c-header-icon logout" onClick={this.handleLogout} ><i className="fa fa-power-off"></i></div>
      </div>
    </div>
  </header>
  <div className="l-sidebar">
    <div className="logo">
      <div className="logo__txt">{username}</div>
    </div>
    <div className="l-sidebar__content">
      <nav className="c-menu js-menu">
        <ul className="u-list">
          <li className="c-menu__item ${} " data-toggle="tooltip" title="Overview" onClick={this.onToggleHome} style={{backgroundColor:this.state.homebg}}>
            <div className="c-menu__item__inner"><i className="fa fa-university"></i>
              <div className="c-menu-item__title"><span>Overview</span></div>
            </div>
          </li>
          <li className="c-menu__item has-submenu" data-toggle="tooltip" title="Vin Check"  onClick={this.onToggleSearch}  style={{backgroundColor:this.state.searchbg}}>
            <div className="c-menu__item__inner"><i className="fa fa-search"></i>
              <div className="c-menu-item__title"><span>Vin Check</span></div>
              <div className="c-menu-item__expand js-expand-submenu"><i className="fa fa-angle-down"></i></div>
            </div>
          </li>

          <li className="c-menu__item has-submenu" data-toggle="tooltip" title="Vin History"  style={{backgroundColor:this.state.historybg}} onClick={this.onToggleHistory}>
            <div className="c-menu__item__inner"><i className="fa fa-bar-chart-o"></i>
              <div className="c-menu-item__title"><span>Vin History</span></div>
            </div>
          </li>
          {/* <li className="c-menu__item has-submenu" data-toggle="tooltip" title="User Profile" onClick={this.onToggleProfile}  style={{backgroundColor:this.state.profilebg}}>
            <div className="c-menu__item__inner"><i className="fa fa-user"></i>
              <div className="c-menu-item__title"><span>User Profile</span></div>
            </div>
          </li> */}
          <li className="c-menu__item has-submenu" data-toggle="tooltip" title="Change Password" onClick={this.onTogglePassword}  style={{backgroundColor:this.state.passwordbg}}>
            <div className="c-menu__item__inner"><i className="fa fa-cogs"></i>
              <div className="c-menu-item__title"><span>Change Password</span></div>
            </div>
          </li>
        </ul>
      </nav>
    </div>
  </div>
</div>
<main className="l-main">
  <div className="content-wrapper content-wrapper--with-bg">
  
    {content}
  </div>
</main>
</div>

    
    )
  }
}

    
const mapStateToProps = state =>{
  return{
    isLoggedIn: state.isUserLoggedIn.isLoggedIn,
    token:state.isUserLoggedIn.token,
    user:state.userInfo,
  };
}

DashBoard.propTypes ={
  UserLogout:PropTypes.func.isRequired,
  FetchUserInfo:PropTypes.func.isRequired,
  isLoggedIn:PropTypes.bool,
  token:PropTypes.string,
  user:PropTypes.any
 
}



export default withRouter(connect(mapStateToProps,{UserLogout,FetchUserInfo})(DashBoard));