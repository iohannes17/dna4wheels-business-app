import React, { Component } from 'react'
import './vincheck.css'
import PropTypes from 'prop-types';
import {fetchVinTeaser} from '../../../../Actions/TeaserActions'
import {getReport} from '../../../../Actions/getReport'
import {connect} from 'react-redux';


const urlimage = 'https://res.cloudinary.com/carfacts-business-services/image/upload/'






    class VinCheck extends Component {

 
    constructor(props){
        super(props);

        this.state={
            vin:'',
        }
        this.onChange= this.onChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
  
  
    onChange(event){
      this.setState({...this.state, [event.target.name]: event.target.value})
    }
  
  
  
  
    handleSubmit(e){
      e.preventDefault();
       const vin = this.state.vin;


       if(vin.length < 0){

        window.alert('Chassis number incorrect');

      }else if(vin.length < 17){

        window.alert('Chassis number less than 17');

      }
      
      else{
      this.props.fetchVinTeaser(vin)
      }
    }



    onDownload=()=>{
      const vin = this.state.vin;
      const token = this.props.token

     
          
        if(parseInt(this.props.data.carfaxRecords) < 0 ||
        this.props.data.carfaxRecords === undefined ||
        this.props.data === undefined ||
        this.props.data === null ||
        this.props.data.carfaxRecords === null){

          window.alert('Carfax Vehicle History not available')
        }
       

        else{
          this.props.getReport(vin,token)
        }
      
    }
      


  render() {

         
       
         let content =<div></div>; 

         const vinn =this.props.data.vin == null ? 'Not Available' : this.props.data.vin;
         
        if(this.props.isAvailable){
            
          content =  
          <div style={{width:'90%',marginTop:20}}>
         
         <div className="teaser-card ">
           <h3 className="headerStyle">Teaser Details</h3>

           <div style={{background:'#339fe7',height:40,}}>
           <p style={{color:'#ffffff'}} className="teaserStyle">VIN/Chassis  :</p>
           <p style={{fontFamily:'Poppins',fontSize:18,color:'#ffffff',right:10,paddingTop:8}}>{vinn}</p>
           </div>

             <div style={{background:'transparent',height:40,}}>
           <p className="teaserStyle">Brand  :</p>
           <p style={{fontFamily:'Poppins',fontSize:18,right:10,paddingTop:8}}>{this.props.data.attributes.Make}</p>
           </div>

             <div style={{background:'#339fe7',height:40}}>
           <p style={{color:'#ffffff'}} className="teaserStyle">Model  :</p>
           <p style={{fontFamily:'Poppins',fontSize:18,color:'#ffffff',right:10,paddingTop: 8,}}>{this.props.data.attributes.Model}</p>
           </div>

            <div style={{background:'transparent',height:40}}>
           <p className="teaserStyle">Year  :</p>
           <p style={{fontFamily:'Poppins',fontSize:18,right:10,paddingTop:8}}>{this.props.data.attributes.Year}</p>
           </div>

               <div style={{background:'#339fe7',height:40,color:'#ffffff'}}>
              <p style={{color:'#ffffff'}} className="teaserStyle">Style  :</p>
              <p style={{fontFamily:'Poppins',fontSize:18,color:'#ffffff',right:10,paddingTop: 8,}}>{this.props.data.attributes.Style}</p>
           </div>

               <div style={{background:'transparent',height:40}}>
           <p className="teaserStyle">Trim  :</p>
           <p style={{fontFamily:'Poppins',fontSize:18,right:20,paddingTop: 8}}>{this.props.data.attributes.Trim}</p>
           </div>
           
           <div style={{background:'#339fe7',height:40,color:'#ffffff'}}>
           <p style={{color:'#ffffff'}} className="teaserStyle">EngineType  :</p>
           <p style={{fontFamily:'Poppins',fontSize:18,color:'#ffffff',right:20,paddingTop: 8}}>{this.props.data.attributes.Engine}</p>
           </div>


             <div style={{background:'transparent',height:40}}>
           <p className="teaserStyle">Country  :</p>
           <p  style={{fontFamily:'Poppins',fontSize:18,right:20,paddingTop: 8}}>{this.props.data.attributes.MadeIn}</p>
           </div>

             <div style={{background:'#339fe7',height:40}}>
           <p style={{color:'#ffffff'}} className="teaserStyle">Class  :</p>
           <p style={{fontFamily:'Poppins',fontSize:18,color:'#ffffff',right:20,paddingTop:8}}>{this.props.data.class}</p>
           </div>

            <div style={{background:'transparent',height:40,}}>
           <p  className="teaserStyle">Record  :</p>
           <p style={{color:'#339fe7',fontFamily:'Poppins',
           fontSize:18,right:20,paddingTop: 8}}>
           {this.props.data.carfaxRecords}</p>
           </div>   


            {this.props.data.isAvailable ?   
           <button
                 onClick={this.onDownload}
                 style={{borderRadius:20,
                   height:50,
                   width:250,
                   marginTop:10,
                   background:'#fec005',
                   color:'#ffffff',
                   fontFamily:'Poppins',
                   fontSize:25
                   }}>Download Report</button> 
                   
                   :

                <button
                 onClick={this.onDownload}
                 style={{borderRadius:20,
                   height:50,
                   width:250,
                   marginTop:10,
                   background:'#fec005',
                   color:'#ffffff',
                   fontFamily:'Poppins',
                   fontSize:25
                   }}>

                   {this.props.isLoadingReport ?  
                    <center><img src ={urlimage+'v1567079330/Dna4wheelsAsset/loaderwheels.png'} className="App-logo" alt="logo"/></center> : 'Get Report'
     }
                   </button>}
        
           </div>
            </div>;

        }

        

                      
                        

    return (
      
          <div style={{alignItems:'center',justifyContent:'center',alignContent:'center'}}>
            <div className="search" style={{alignSelf:'center'}}>
                <input type="text" className="searchTerm" placeholder="Enter VIN/Chassis" maxLength={17} name="vin" value={this.state.vin} onChange={this.onChange}/>
                <button type="submit" className="searchButton" onClick={this.handleSubmit}>
                     {this.props.isGettingTeaser
                       ? <img src ={urlimage+'v1567079330/Dna4wheelsAsset/loaderwheels.png'} className="inTeaserApp-logo" alt="logo"/>
                       : <i className="fa fa-search"></i>}
                </button>
            </div>
            <center>{content}</center>
       </div>
        
    
    )
  }
}


    
const mapStateToProps = state =>{
    return{
       isFetching: state.teaserVin.isFetching,
       isAvailable: state.teaserVin.isAvailable,
       data:state.teaserVin.teaserList,
       isGettingTeaser:state.teaserVin.teaserRequest,
       data_two:state.teaserVin.teaserListTwo,
       token:state.isUserLoggedIn.token,
       isLoadingReport:state.vhrreport.isLoading
    };
  }
  
  VinCheck.propTypes ={
    fetchVinTeaser:PropTypes.func.isRequired,
    isFetching:PropTypes.bool,
    isAvailable:PropTypes.bool,
    data:PropTypes.any,
    data_two:PropTypes.any,
    getReport:PropTypes.func.isRequired,
    token:PropTypes.string.isRequired,
    isLoadingReport:PropTypes.bool,
    isGettingTeaser:PropTypes.bool
  }
  
  
  
  export default connect(mapStateToProps, {fetchVinTeaser,getReport})(VinCheck);
