import React, { Component } from 'react'
import './overview.css'
import {connect} from 'react-redux';
import PropTypes from 'prop-types';









 class Overview extends Component {

    


  render() {

    const fname =  this.props.user.userInfoList.first_name;
    const lname =  this.props.user.userInfoList.last_name;
    const description =  this.props.user.userInfoList.description;
    const address = this.props.user.userInfoList.address_line_1  == null ? '' : this.props.user.userInfoList.address_line_1 ;;
    const address2 = this.props.user.userInfoList.address_line_2 == null ? '' : this.props.user.userInfoList.address_line_2 ;
    const total = this.props.vinHist.vinhistoryList.total;
  
    
   

    return (
                    
                <div className="row">
                <div className="column cardtilesOverview">
               
                    <div className="card">
                    <i className="fa fa-university fa-3x iconistyle"/>
                    <h3 className="overviewHeader">Overall Total Checks</h3>
                    <p className="overviewinfo">{total}</p><br/>
                    <p className="overviewsubText"></p>
                    </div>
                   
                </div>

                <div className="column cardtilesOverview">
                    <div className="card">
                    <i className="fa fa-user fa-3x iconistyle"/>
                    <h3 className="overviewHeader">User Profile</h3>
                    <p className="overviewinfo">{fname +' '+ lname}</p>
                    <p className="overviewsubText">{description}</p>
                    <p className="overviewsubText">{address +' '+ address2}</p>
                   
                    </div>
                </div>
                
              
                </div>

    )
  }
}

   
const mapStateToProps = state =>{
    return{
      
      user:state.userInfo,
      vinHist:state.vinInfo,
      token:state.isUserLoggedIn.token,
    };
  }
  
  Overview.propTypes ={
    user:PropTypes.any,
    vinHist:PropTypes.any,
    token:PropTypes.string,
   
  }
  
  
  
  export default connect(mapStateToProps,{})(Overview);
