import React from 'react';
import PropTypes from 'prop-types';
import './changePass.css';
import {connect} from 'react-redux';
import {changePassword} from '../../../../Actions/changePassAction'




class PasswordChange extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            oldPassword:'',
            newPassword:'',
            confirmpass:''
        };
        this.onChange= this.onChange.bind(this);
       
      }
  
  
    onChange(event){
      this.setState({...this.state, [event.target.name]: event.target.value})
    }


    onRestPassword = ()=>{
        const oldpass = this.state.oldPassword;
        const newp = this.state.newPassword;
        const cnewp= this.state.confirmpass;
        const token = this.props.token

        if(newp === cnewp){

             const data ={
                
                 old_password:oldpass,
                 password_confirmation:cnewp,
                 password:newp,
             }

           this.props.changePassword(data,token)


        }else{

            window.alert('Passwords dont match')
        }
    }




    render() {
        return (
            <div>
                  <div className="password one-edge-shadow">
              <div className="password-triangle"></div>
              <h2 className="password-header">Change Password</h2>

              <form className="password-container">
              <p><input type="password" placeholder="Old Password" value={this.state.oldPassword} name="oldPassword" onChange={this.onChange}/></p>
              <p><input type="password" placeholder="New Password" value={this.state.newPassword} name="newPassword" onChange={this.onChange}/></p>
              <p><input type="password" placeholder="Confirm New Password" value={this.state.confirmpass} name="confirmpass" onChange={this.onChange}/></p>
            
            
              {this.props.isFetching ? 
              'loading'
              : 
              <p><input type="submit" value="Change" onClick={this.onRestPassword}/></p>}
              </form>
              </div>

            </div>
        );
    }
}

const mapStateToProps = state =>{
    return{
        isFetching: state.passchange.isLoading,
        token:state.isUserLoggedIn.token,
    };
  }

  
  PasswordChange.propTypes ={  
      changePassword:PropTypes.func.isRequired,
      isFetching:PropTypes.bool,
      token:PropTypes.string,
  }
  
  
  
  export default connect(mapStateToProps, {changePassword})(PasswordChange);