import React from 'react';
import PropTypes from 'prop-types';
import './notification.css'
import {connect} from 'react-redux';
import {NotificationHistory} from '../../../../Actions/NotificationActions'


class Notifications extends React.Component {





    componentDidMount(){

        const token = this.props.token;
        this.props.NotificationHistory(token)
    }




    render() {

           const data = this.props.data;

        return (
            <div>  
            {data.map(x =>        
                 <div className="divStyle">
                     <p style={styles.title}>{x.title}</p>
                       <p style={styles.body}>{x.body}</p>
                </div>
            
              
           )}</div>
        );
    }
}


const mapStateToProps= state =>{
    return{
        token:state.isUserLoggedIn.token,
        data:state.notification.data
    }
}

Notifications.propTypes = {
  NotificationHistory:PropTypes.func.isRequired,
  token:PropTypes.string,
  data:PropTypes.array

};

export default connect(mapStateToProps,{NotificationHistory}) (Notifications);


const styles={

    title:{
       fontFamily:'Poppins',
       fontSize:18,
       fontWeight:"900",
       fontStyle:'normal',
       color:'black'
    },

    body:{
        fontFamily:'Poppins',
       fontSize:16,
       fontWeight:"700",
       fontStyle:'normal',
       color:'black'
    }
}