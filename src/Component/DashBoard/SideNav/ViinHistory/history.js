import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import './history.css'



class VinHistory extends React.Component {
    
    
    render() {

        const data = this.props.vinList.data;



        return (

            <div>
                  <table className="tablestyle"
                   style={{width:'100%', alignItems:'center', }}>
                   <tbody>
                            <tr>
                <th style={styles.headertext}>Date Checked</th>
                <th style={styles.headertext}>Chassis</th>
                    </tr>
                    </tbody>
                                
                      {data.map(x => 
                         
                             <tbody>
                                 <tr>
                            <td style={styles.subtext} key={x.id}>{x.created_at}</td>
                            <td style={styles.subtext}  key={x.id}> {x.vin}</td>
                            </tr>
                            </tbody>
                        )}
                 </table>
              </div>
        );
    }
}




const mapStateToProps=(state)=>{
    return{
        vinList: state.vinInfo.vinhistoryList
    }
}

VinHistory.propTypes = {
    vinList:PropTypes.any
};

export default connect(mapStateToProps,{}) (VinHistory);

const styles={

    headertext:{
        textAlign:'center',
        fontFamily:'Poppins',
        fontWeight:"bolder",
        fontSize:15
    },

    subtext:{
        textAlign:'center',
        fontFamily:'Poppins',
        fontWeight:"500",
        fontSize:12,
        padding:5
    },

}
