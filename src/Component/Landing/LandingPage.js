import React from 'react'
import './LandingPage.css';
import PropTypes from 'prop-types';
import {UserLogin} from '../../Actions/loginActions';
import {connect} from 'react-redux';
import {withRouter} from 'react-router'
import {Link} from 'react-router-dom'


const urlimage = 'https://res.cloudinary.com/carfacts-business-services/image/upload/'





class LandingPage extends React.Component {

    constructor(props){
      super(props);
      this.state={
        email:'',
        password:'',
        output:''
        
      }
      this.onChange= this.onChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }


  onChange(event){
    this.setState({...this.state, [event.target.name]: event.target.value})
  }




  handleSubmit(e){
    e.preventDefault();
    const loginParam ={ 
      username: this.state.email,
      password: this.state.password
    }

    this.props.UserLogin(loginParam);
    
  }



   render() {
    return (                            
    <div>
      <div className="row">
      <div className="col-md-6 left">
      <div className="bs-example">
      <div id="myCarousel" className="carousel slide" data-interval="5000" data-ride="carousel">

        <ol className="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" className="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="4"></li>
            <li data-target="#myCarousel" data-slide-to="5"></li>
        </ol>   
     
        <div className="carousel-inner">

            <div className="active item carousel-fade">
                <img src={urlimage+'v1566990074/Dna4wheelsAsset/min3.png'} style={{height:'100%'}} alt="slide1"/>
            </div>

            <div className="item carousel-fade">
            <img src={urlimage+'v1566990072/Dna4wheelsAsset/min2.png'} style={{height:'100%'}} alt="slide2"/>
            </div>

            <div className="item carousel-fade">
                <img src={urlimage+'v1566990058/Dna4wheelsAsset/1-min.png'} style={{height:'100%'}} alt="slide3"/>
            </div>

              <div className="item carousel-fade">
            <img src={urlimage+'v1566990054/Dna4wheelsAsset/min4.png'} style={{height:'100%'}} alt="slide4"/>
            </div>
            
            <div className="item carousel-fade">
                <img src={urlimage + 'v1566990085/Dna4wheelsAsset/min5.png'} style={{height:'100%'}} alt="slide5"/>
            </div>
        </div>
      

    </div>
</div>
      
                         </div>

              <div className="col-md-6 right">
             
              <div className="login one-edge-shadow">
              <center><img src ={urlimage+'v1566990000/Dna4wheelsAsset/dna_j.png'} className="logologin" alt="logo"/></center>
              <div className="login-triangle"></div>
              <h2 className="login-header">Log in</h2>

              <form className="login-container">
              <p><input type="email" placeholder="Username" value={this.state.email} name="email" onChange={this.onChange}/></p>
              <p><input type="password" placeholder="Password" value={this.state.password} name="password" onChange={this.onChange}/></p>
              {this.props.isFetching ? 
               <center><img src ={urlimage+'v1567079330/Dna4wheelsAsset/loaderwheels.png'} className="App-logo" alt="logo"/></center>
              : 
              <p><input type="submit" value="Log in" onClick={this.handleSubmit}/></p>
              }
               
               <p><Link className="text-danger" to='./fpassword'>Forgot Password?</Link></p>
              </form>
               {this.props.isCorrect === '' ? ' ': <p className="text-danger">incorrect password or username</p>}
              </div>

              </div>
              </div>
                </div>
                )
                
            }
        }

    
const mapStateToProps = state =>{
  return{
      isLoggedIn: state.isUserLoggedIn.isLoggedIn,
      isFetching: state.isUserLoggedIn.isFetching,
      isCorrect: state.isUserLoggedIn.isCorrect,
      token:state.isUserLoggedIn.token
     
  };
}

LandingPage.propTypes ={
  UserLogin: PropTypes.func.isRequired,
  isLoggedIn:PropTypes.bool.isRequired,
  isFetching:PropTypes.bool.isRequired,
  isCorrect:PropTypes.string,
  token: PropTypes.string
}



export default withRouter(connect(mapStateToProps, {UserLogin})(LandingPage));