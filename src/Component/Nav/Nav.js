import React from 'react';
  import './Nav.css';
  import dna from '../../assets/images/dna.png'
  import {Link} from 'react-router-dom'



export default class NavbarPage extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
      render() {

        return (
         
         <div className="row navi navcolor">
         <div className="col-6">
           <img src ={dna} className="logo-style" alt="logo"/>
         </div>
         <div className="col-6">
         <div className="linkk float-right">
          <Link to="/" className="btStyle">Home</Link>
          <Link to="/service" className="btStyle">Services</Link>
          <Link to="/contact" className="btStyle">Contact</Link>
          <Link to="/pricing" className="btStyle">Pricing</Link>
          <Link to="/about" className="btStyle">About</Link>
          <Link to="/register" className="btStyle">Register</Link>
          
         </div>
         </div>

         </div>
                              
        );
      }
    }
