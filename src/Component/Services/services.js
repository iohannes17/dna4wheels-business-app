import React, { Component } from 'react'
import './services.css'
const urlimage = 'https://res.cloudinary.com/carfacts-business-services/image/upload/'


export default class Service extends Component {
  render() {

    return (
                    
                <div className="row">
                <div className="column cardtiles">
                    <div className="card">
                    <img src ={urlimage+'v1566990072/Dna4wheelsAsset/report.png'} className="iconlogo text-center" alt="logo" style={{position:"absolute",top:-100,zIndex:100,alignSelf:'center'}}/>
                    <div style={{height:35}}></div>
                    <h3 style={styles.headerText}>Vehicle History Report</h3>
                    <p className="text-italic" style={styles.subtext}>Over 20 billion vehicle history reports</p>
                    <p style={styles.lastText}>Dna4Wheels Local Report <br/> Foreign Report</p>
                    <p style={styles.lastText}></p>
                    </div>
                </div>

                <div className="column cardtiles">
                    <div className="card">
                    <img src ={urlimage+'v1566989998/Dna4wheelsAsset/inspection_ktfgfa.png'} className="iconlogo text-center" alt="logo" style={{position:"absolute",top:-100,zIndex:100,alignSelf:'center'}}/>
                    <div style={{height:35}}></div>
                    <h3 style={styles.headerText}>Vehicle Inspection</h3>
                    <p style={styles.subtext}>Comprehensive diagnostic checks on vehicles, on request</p>
                    <p  style={styles.lastText}>We offer top notch vehicle investigative services</p>
                  
                    </div>
                </div>
                
                <div className="column cardtiles">
                    <div className="card">
                    <img src ={urlimage+'v1566990028/Dna4wheelsAsset/maintenance.png'} className="iconlogo text-center" alt="logo" style={{position:"absolute",top:-100,zIndex:100,alignSelf:'center'}}/>
                    <div style={{height:35}}></div>
                    <h3 style={styles.headerText}>Vehicle Maintenece/Repairs</h3>
                    <p style={styles.subtext}> From custom body works to regular maintenance and repairs</p>
                    <p style={styles.lastText}>We provide the best auto mechanic service at the <strong>Car Spa</strong> </p>
                    
                    </div>
                </div>


                   
                   <div className="column cardtiles">
                    <div className="card">
                    <img src ={urlimage+'v1566990061/Dna4wheelsAsset/purchase.png'} className="iconlogo text-center" alt="logo" style={{position:"absolute",top:-100,zIndex:100,alignSelf:'center'}}/>
                    <div style={{height:35}}></div>
                    <h3 style={styles.headerText}>Vehicle Financing</h3>
                    <p style={styles.subtext}>Making vehicle purchasing easier and Enjoy the lowest interest rate</p>
                    <p style={styles.lastText}>Own your dream car</p>
                   
                    </div>
                </div>

                 <div className="column cardtiles">
                    <div className="card">
                    <img src ={urlimage+'v1567006326/Dna4wheelsAsset/car_purchase.png'} className="iconlogo text-center" alt="logo" style={{position:"absolute",top:-100,zIndex:100,alignSelf:'center'}}/>
                    <div style={{height:35}}></div>
                    <h3 style={styles.headerText}>Vehicle Purchasing</h3>
                    <p style={styles.subtext}>Our experts help you buy the perfect vehicle at the best possible price</p>
                    <p style={styles.lastText}>Transparency in purchase process</p>
                    
                    </div>
                </div>
                </div>

    )
  }
}


const styles = {

    headerText:{
        color:'#339fe7',
        fontSize:23,
        fontFamily:'Poppins',
        marginTop:10,
        fontWeight:"900"
    },

    subtext:{
        color:'#000000',
        fontSize:10,
        fontFamily:'Poppins',
        fontWeight:"200",
        fontStyle:'italics',
        
    },

    lastText:{
        color:'#000000',
        fontFamily:'Poppins',
        fontSize:14,
        fontWeight:"900",
        marginTop:10
      
    }
}
