import React, { Component } from 'react';
import './About.css';


class About extends Component {
    render() {
        return (
           
            <div class="container">
           
   
    
    <h1 class="my-5 h3 text-center" id="packageHeader">Welcome to Dna4Wheels</h1>

    
    <div class="row features-small mt-5 wow fadeIn">
    
    <p class="text-justify">Dna4Wheels (www.Dna4Wheels.ng) is the official Dna4Wheels website in Nigeria and the only legal source 
                for authentic Vehicle History Reports. These reports are available for all used cars
                 imported from the U.S. and Canada and manufactured after 1981 and in Nigeria..</p>

            <p class="text-justify">Dna4Wheels is the most trusted provider of vehicle history information in Nigeria. 
                For over 3 years, Dna4Wheels has helped millions of used car shoppers make better purchase decisions 
                and avoid costly hidden problems.</p>
                <br/>

            <p class="text-justify">Dna4Wheels has been providing vehicle history information in North America for decades 
                and currently has a data base with more than 14 billion records collected from 35.000 partners 
                and sources, including government ministries, law enforcement agencies, service and repair shops, 
                car dealers, insurance companies and many others. Dna4Wheels is the neutral partner of consumers, dealers 
                and partners from the private and public sectors – having the vision of changing the used car market
                 for the better. No used car from the U.S. should be purchased without checking its history first with
                  a Dna4Wheels Report.</p>
    </div>  
             
         </div>
    
            
        );
    }
}

export default About;