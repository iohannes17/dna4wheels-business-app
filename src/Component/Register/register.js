import React from 'react';
import './register.css'
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {RegisterUser} from '../../Actions/registerActions'


class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          username:'',
          fname:'',
          lname:'',
          email:'',
          company:'',
          cac:'',
          city:'',
          password:'',
          password_confirmation:'',
          phone:'',
          news:false,
          terms:undefined,
          address_line_1:'',
          stated:'',
          website:'',
          business_description:'',
          taxId:''
        };
        this.onChange= this.onChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
  
  
    onChange(event){
      this.setState({...this.state, [event.target.name]: event.target.value})
    }
  
  
  
  
    handleSubmit(e){
      e.preventDefault();
     
       
      if(
        this.state.username.length < 1 ||
        this.state.fname.length < 1 ||
        this.state.lname.length < 1 ||
        this.state.email.length < 1 ||
        this.state.company.length < 1 ||
        this.state.cac.length < 1 ||
        this.state.city.length < 1 ||
        this.state.password.length < 1 ||
        this.state.password_confirmation.length < 1 ||
        this.state.phone.length < 1 ||
        this.state.address_line_1.length < 1 ||
        this.state.stated.length < 1 ||
        this.state.website.length < 1 ||
        this.state.business_description.length < 1 ||
        this.state.taxId.length < 1
      )
      {
        window.alert('Please fill all field')
      }

      else{

            
        if(this.state.terms === 'on'){

             
          const registerParam ={
            username: this.state.username,
            email: this.state.email ,
            email1: this.state.email ,
            first_name:this.state.fname,
            last_name:this.state.lname,
            phone: this.state.phone,
            password: this.state.password,
            password_confirmation: this.state.password_confirmation,
            address_line_1:this.state.address_line_1,
            city:this.state.city,
            state:this.state.stated,
            business_name:this.state.company,
            rc_number:this.state.cac,
            tax_identification_number: this.state.taxId,
            business_description:this.state.business_description,
            website:this.state.website,
            frontend_url:'https://carfax.ng/#/activate/'
          }
           this.props.RegisterUser(registerParam);
           console.log(registerParam);
        }


       else{
        window.alert('Accept the terms and conditions')
    }
   

      }
        
     
    }

    


    render() {
        return (
            <div className="form_wrapper">
     <div className="form_container">
      <div className="title_container">
      <h2>Business Registration</h2>
    </div>
    <div className="row clearfix">
      <div className="">
        <form>
        <div className="input_field"> <span><i aria-hidden="true" className="fa fa-user"></i></span>
            <input type="text" name="username" placeholder="Username" required   value={this.state.username} onChange={this.onChange}/>
          </div>

        <div className="input_field"> <span><i aria-hidden="true" className="fa fa-user"></i></span>
        <input type="text" name="fname" placeholder="First Name" required   value={this.state.fname} onChange={this.onChange}/>
          </div>



        <div className="input_field"> <span><i aria-hidden="true" className="fa fa-user"></i></span>
        <input type="text" name="lname" placeholder="Last Name" required   value={this.state.lname} onChange={this.onChange}/>
          </div>


          <div className="row clearfix">
         
             <div className="col_half">
              <div className="input_field"> <span><i aria-hidden="true" className="fa fa-phone"></i></span>
                <input type="text" name="phone" placeholder="Phone" required   value={this.state.phone} onChange={this.onChange}/>
              </div>
            </div>
          </div>

          <div className="input_field"> <span><i aria-hidden="true" className="fa fa-envelope"></i></span>
            <input type="email" name="email" placeholder="Email" required   value={this.state.email} onChange={this.onChange}/>
          </div>
          <div className="input_field"> <span><i aria-hidden="true" className="fa fa-user"></i></span>
            <input type="text" name="company" placeholder="Company Name" required   value={this.state.company} onChange={this.onChange}/>
          </div>
          <div className="input_field"> <span><i aria-hidden="true" className="fa fa-globe"></i></span>
            <input type="text" name="website" placeholder="Website" required  value={this.state.website} onChange={this.onChange}/>
          </div>
          <div className="input_field"> <span><i aria-hidden="true" className="fa fa-lock"></i></span>
            <input type="password" name="password" placeholder="Password" required  value={this.state.password} onChange={this.onChange}/>
          </div>
          <div className="input_field"> <span><i aria-hidden="true" className="fa fa-lock"></i></span>
            <input type="password" name="password_confirmation" placeholder="Re-type Password" required  value={this.state.password2} onChange={this.onChange}/>
          </div>
          <div className="input_field"> <span><i aria-hidden="true" className="fa fa-home"></i></span>
            <input type="text" name="address_line_1" placeholder="Address"  value={this.state.address_line_1} onChange={this.onChange}/>
          </div>
         

            <div className="row clearfix">
            <div className="col_half">
              <div className="input_field"> <span><i aria-hidden="true" className="fa fa-map-marker"></i></span>
                <input type="text" name="city" placeholder="City"  value={this.state.city} onChange={this.onChange} />
              </div>
            </div>
            <div className="col_half">
              <div className="input_field"> <span><i aria-hidden="true" className="fa fa-map-pin"></i></span>
                <input type="text" name="stated" placeholder="State" required  value={this.state.stated} onChange={this.onChange}/>
              </div>
            </div>
            <div className="col_half">
              <div className="input_field"> <span><i aria-hidden="true" className="fa fa-vcard-o"></i></span>
                <input type="text" name="taxId" placeholder="Tax Id" required   value={this.state.taxId} onChange={this.onChange}/>
              </div>
            </div>
            <div className="col_half">
              <div className="input_field"> <span><i aria-hidden="true" className="fa fa-folder-o"></i></span>
                <input type="text" name="cac" placeholder="CAC/RC Number" required   value={this.state.cac} onChange={this.onChange}/>
              </div>
            </div>
          </div>
            	<div className="input_field radio_option">
                  <textarea rows={7} value={this.state.business_description} onChange={this.onChange} placeholder="Business Description"
                   style={{width:'100%',background:'#ffffff'}} name="business_description"/>
              </div>
             
            <div className="input_field checkbox_option">
            	<input type="checkbox" id="cb1" name="terms"  checked={this.state.terms} onChange={this.onChange}/>
    			<label htmlFor="cb1">I agree with terms and conditions</label>
            </div>
            <div className="input_field checkbox_option">
            	<input type="checkbox" id="cb2" name="news" checked={this.state.news} onChange={this.onChange}/>
    			<label htmlFor="cb2">I want to receive the newsletter</label>
            </div>
          <input className="button" type="submit" value="Register" onClick={this.handleSubmit}/>
        </form>
      </div>
    </div>
  </div>
</div>
        );
    }
}

    
const mapStateToProps = state =>{
  return{
     
     
  };
}

Register.propTypes ={
  RegisterUser: PropTypes.func.isRequired,
  
 
}



export default connect(mapStateToProps, {RegisterUser})(Register);