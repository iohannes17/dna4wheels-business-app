import React from 'react';
import './fpassword.css'
import PropTypes from 'prop-types';
import {RetrievePassword} from '../../Actions/registerActions'
import {connect} from 'react-redux';



class ForgotPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email:'',
            info:''
        };

        this.onChange = this.onChange.bind(this)
    }



  onChange(event){
    this.setState({...this.state, [event.target.name]: event.target.value})
  }




    onPressGetPassword = () =>{
        
    const email = this.state.email;
    const frontend = 'https://carfax.ng/#/reset-password/';

let testMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

   
     
    if(email.length < 0 || email === ''){
        // window.confirm('Empty email' change-password)
        window.alert('Please fill email field')


    }
    else if(testMail.test(email))
    {
        const fPassword ={
            email: email ,
            frontend_url:frontend
          }
           this.props.RetrievePassword(fPassword);
    }

    else { window.alert('Email incorrect')}

  }




    render() {


     

        return (
            <div>
              <div className="fpass one-edge-shadow">
              <div className="fpass-triangle"></div>
              <h2 className="fpass-header">Forgot Password</h2>

              <form className="fpass-container">
              <p><input type="email" placeholder="Email" value={this.state.email} name="email" onChange={this.onChange}/></p>
              {this.props.isLoadingPassword ? 
              this.props.passRes === '' ? 'Loading' : this.props.passRes
              : 
              <p><input type="submit" value="Reset" onClick={this.onPressGetPassword}/></p>
              }
            
              </form>
              </div>

              </div>
        );
    }
}

const mapStateToProps = state =>{
    return{
       
        passRes: state.dataReg.forgotPassSuccess,
        isLoadingPassword:state.dataReg.isLoadingPassword     
    };
  }
  

  ForgotPassword.propTypes ={
    RetrievePassword: PropTypes.func.isRequired,
    isLoadingPassword:PropTypes.bool   
  }
  
  
  
  export default connect(mapStateToProps, {RetrievePassword})(ForgotPassword);