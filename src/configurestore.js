import { createBrowserHistory } from 'history'
import { applyMiddleware, compose, createStore } from 'redux'
import { routerMiddleware } from 'connected-react-router'
import rootReducer from './reducers'
import thunk from 'redux-thunk'
import {history} from './history'


export default function configureStore(preloadedState) {
  const store = createStore(
    rootReducer, // root reducer with router state
    preloadedState,
    compose(
      applyMiddleware(
          thunk,
        routerMiddleware(history), // for dispatching history actions
        // ... other middlewares ...
      ),
    ),
  )

  return store
}