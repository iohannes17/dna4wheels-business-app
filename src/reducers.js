import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import loginReducer from './Reducers/Login'
import logoutReducer from './Reducers/Logout';
import  UserInfoReducer from './Reducers/userInfoReducer'
import fetchVinHistoryReducer from './Reducers/fetchVinReducer'
import fetchVinTeaserReducer from './Reducers/TeaserReducer';
import registerReducer from './Reducers/registerReducer'
import NotificationReducer from './Reducers/NotificationReducer';
import changePassReducer from './Reducers/changePassReducer';
import  getReportReducer from './Reducers/getReport';
import downloadReportReducer from './Reducers/downloadReport'
import {LOGOUT_SUCCESS} from './Constants/Constants'
import {history} from './history'




const appReducer = combineReducers({
  router: connectRouter(history),
  isUserLoggedIn:loginReducer,
  logOut: logoutReducer,
  userInfo: UserInfoReducer,
  vinInfo: fetchVinHistoryReducer,
  teaserVin:fetchVinTeaserReducer,
  dataReg:registerReducer,
  notification:NotificationReducer,
  passchange:changePassReducer,
  vhrreport:getReportReducer,
  downloadReport: downloadReportReducer
   
});

const rootReducer = (state,action)=>{
  if(action.type === LOGOUT_SUCCESS){
    state = undefined
  }

  return appReducer(state,action)
}

export default rootReducer;
