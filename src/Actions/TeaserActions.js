/**
 * @export fetchVinTeaser
 * @export fetchVinTeaserError
 * @export fetchVinTeaserSuccess
 * @export AnalyzerError
 * @export AnalyzerSuccess
 * @export RiskAnalyzer
 * @export RESETVIN
 * 
 */


import {TEASER_REQUEST,TEASER_ERROR,TEASER_SUCCESS} from '../Constants/Constants';


/**
 * @function fetchVinTeaserSuccess
 * @param {string} json
 * @return JSON data
 */

export const fetchVinTeaserSuccess =json => ({
     type:TEASER_SUCCESS,
     payload:json,
});



/**
 * @function fetchVinTeaserError
 * @param {string} error
 * @return string data
 */

export const fetchVinTeaserError = error => ({
  type: TEASER_ERROR,
  payload: error,
});



export const RequestTeaser = ()=>({type: TEASER_REQUEST});
 

  /**
   * @function fetchVinTeaser
   * @param {string} vin
   * @return {JSON} response
   * Retrieves VIN teaser details from API 
   */

    export const fetchVinTeaser = (vin) =>{
          

      return async dispatch =>{
        try{

          dispatch(RequestTeaser());
          let response = await fetch("https://api.carfacts.ng/api/vincheck/" + vin
    
        );
         let json = await response.json();    
          

        
          dispatch(fetchVinTeaserSuccess(json.data));
        
           
      

        
        }catch(error){
           dispatch(fetchVinTeaserError(error));
        }
      }}

