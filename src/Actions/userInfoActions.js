/**
 * @export FetchUserInfo
 * Retrieves logged in user information
 */

import {FETCH_USERINFO_ERROR,FETCH_USERINFO_SUCCESS,FETCH_VIN_ERROR,FETCH_VIN_SUCCESS} from '../Constants/Constants';




/**
 * 
 * @param {String} json 
 * dispatches JSON response from API to the reducer
 */
export const UserInfoSuccess = json => ({
     type:FETCH_USERINFO_SUCCESS,
     payload:json,
});

/**
 * @param {String} error
 * dispatches error message to the reducer
 */

export const UserInfoError = error => ({
  type: FETCH_USERINFO_ERROR,
  payload: error,
});


/**
 * @function FetchUserInfo
 * @return {JSON}
 * Retrieves LoggedIn User Information
 */

  export const FetchUserInfo = (token) =>{
    
    return async dispatch =>{

      try{
         
  
          
       let response = await fetch('https://api.carfacts.ng/api/users/me',{
        method:'GET',
        headers:{
          'content-type': 'application/json',
          'Authorization': 'Bearer '+ token
        },
      });
      
       let json = await response.json();
          
       
        dispatch(UserInfoSuccess(json.business));
          
        let response2 = await fetch('https://api.carfacts.ng/api/vinhistory/' + json.business.user_id,{
          method:'GET',
          headers:{
          'content-type': 'application/json',
          'Authorization': 'Bearer '+ token
          },
        
      });
            let json2 = await response2.json();
           
            dispatch(fetchVinHistorySuccess(json2));
    

      //  else if(json.business){
      //   dispatch(UserInfoSuccess(json.business));
              
      //   console.log(json.business.user_id + 'here it is' )
      //   let response = await fetch('https://api.carfacts.ng/api/vinhistory/' + json.business.user_id,{
      //     method:'GET',
      //     headers:{
      //     'content-type': 'application/json',
      //     'Authorization': 'Bearer '+ token
      //     },
        
      // });
      //       let json = await response.json();
      //       dispatch(fetchVinHistorySuccess(json));
            
          
         
       
      //  }    
 
      }catch(error){
        dispatch(UserInfoError(error));
        dispatch(fetchVinHistoryError(error))
      }
    
    };
   } ;




export const fetchVinHistorySuccess =json => ({
  type:FETCH_VIN_SUCCESS,
  payload:json,
});

export const fetchVinHistoryError = error => ({
type: FETCH_VIN_ERROR,
payload: error,
});
 
 
