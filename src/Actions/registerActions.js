/**
 * @export RegisterUser
 * @export RetrievePassword
 */

import {REGISTER_REQUEST,REGISTER_SUCCESS,REGISTER_ERROR,FORGOT_PASSWORD_SUCCESS,FOGOT_PASSWORD_ERROR,PASSWORD_REQUEST} from '../Constants/Constants';




/**
 * Called when user initiates a registration request
 */
export const RegisterRequest = () =>({
    type: REGISTER_REQUEST,
    payload:'',
});


/**
 * Called when user initiates a password recovery request
 */

export const PaswordRequest = () =>({
  type:PASSWORD_REQUEST,
  payload:'',
});


/**
 * Called when registration is successful
 * @param {String} json
 */

export const RegisterSuccess = json=> ({
     type: REGISTER_SUCCESS,
     payload:json,
});


/**
 * Called when registration fails
 * @param {String} error
 */

export const RegisterError = (error) => ({
  type: REGISTER_ERROR,
  payload: error,
});



/**
 * Called when password recovery is successful
 * @param {String} json
 */

export const ForgotPasswordSuccess = (json) => ({
  type: FORGOT_PASSWORD_SUCCESS,
  payload:json,
});



/**
 * Called when password recovery fails
 * @param {String} error 
 */
export const ForgotPasswordError = (error) => ({
type: FOGOT_PASSWORD_ERROR,
payload: error,
});







/**
 * @function RegisterUser
 * @param {JSON} userData
 * @return {String} response
 * returns a status 201(OK) from successful registration
 */

export const RegisterUser = userData => dispatch =>{

     RegisterRequest();
   
    fetch('https://api.carfacts.ng/api/auth/business/signup',{

      method:'POST',
      headers:{
        'content-type': 'application/json',
      },

      body: JSON.stringify(userData)

    }).then(res => {
            
     
    })
    .catch((error) => {
      
         dispatch(RegisterError(error)); 
      });
  } 





/**
 * @function RetrievePassword
 * @param {String} email
 * @return {String} response
 * returns a status 200(OK) from successful registration
 */

  export const RetrievePassword = email => dispatch =>{

    dispatch(PaswordRequest());
  
   fetch('https://api.carfacts.ng/api/auth/password/recovery',{
     method:'POST',
     headers:{
       'content-type': 'application/json',

     },
     body:JSON.stringify(email)
   })
   . then(res => {
          const statusCode = res.status;
        if(statusCode === 200){
          dispatch(ForgotPasswordSuccess('ok'))
          window.alert('Email Reset : Check your email')
        } 
        else{
          dispatch(ForgotPasswordError('error'))
          window.alert('Error Resetting Password')
        }
       
        
            
        })
        .catch((error) => {
                  dispatch(ForgotPasswordError(error)); 
          });
      } 





          

  