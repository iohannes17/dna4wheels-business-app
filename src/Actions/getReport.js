/**
 * @export getReprt
 * 
 */

import {REPORT_ERROR,REPORT_REQUEST,REPORT_SUCCESS} from '../Constants/Constants';




/**
 * Called when request
 */
export const getReportRequest = () =>({
    type: REPORT_REQUEST,
    payload:'',
});


/**
 * Called when report is successful
 * @param {String} json
 */

export const getReportSuccess = json=> ({
     type:REPORT_SUCCESS,
     payload:json,
});


/**
 * Called when report request change fails
 * @param {String} error
 */

export const getReportError = (error) => ({
  type: REPORT_ERROR,
  payload: error,
});





/**
 * @function getReport
 * @param {String} email
 * @return {String} response
 * returns a status 200(OK) from successful registration
 */

export const getReport = (vin,token) =>{
    
    return async dispatch =>{

      try{
      
          
            dispatch(getReportRequest());
           
          
       let response = await fetch('https://api.carfacts.ng/api/vin/'+ vin,{
        method:'GET',
        headers:{
          'content-type': 'application/json',
          'Authorization': 'Bearer '+ token
        },
      });
    
      let json = await response.json();
        const endpoint = json.file    
        setTimeout(()=>window.open(endpoint,json.data.vin),3000)
        setTimeout(()=> dispatch(getReportSuccess('correct')),2000)
        
      
      

    }catch(error){
       dispatch(getReportError(error))
    }
}}

          

  