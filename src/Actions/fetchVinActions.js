import {FETCH_VIN_SUCCESS,FETCH_VIN_ERROR} from '../Constants/Constants';



export const fetchVinHistorySuccess =json => ({
     type:FETCH_VIN_SUCCESS,
     payload:json,
});

export const fetchVinHistoryError = error => ({
  type: FETCH_VIN_ERROR,
  payload: error,
});


export const fetchVinHistory = (id,value) =>{
    
   return async dispatch =>{
     try{

      
    let response = await fetch('https://api.carfacts.ng/api/vinhistory/' + id,{
      method:'GET',
      headers:{
      'content-type': 'application/json',
      'Authorization': 'Bearer '+ value
      },
    
  });
        let json = await response.json();
        dispatch(fetchVinHistorySuccess(json));
       
      
     

     }catch(error){
        dispatch(fetchVinHistoryError(error));
     }
   
   };
  } ;



