/**
 * @export getReprt
 * 
 */

import {DOWNLOAD_ERROR,DOWNLOAD_REQUEST,DOWNLOAD_SUCCESS} from '../Constants/Constants';




/**
 * Called when user request download
 */
export const downloadReportRequest = () =>({
    type: DOWNLOAD_REQUEST,
    payload:'',
});


/**
 * Called when report is successful
 * @param {String} json
 */

export const downloadReportSuccess = json=> ({
     type:DOWNLOAD_SUCCESS,
     payload:json,
});


/**
 * Called when report request change fails
 * @param {String} error
 */

export const downloadReportError = (error) => ({
  type: DOWNLOAD_ERROR,
  payload: error,
});





/**
 * @function getReport
 * @param {String} email
 * @return {String} response
 * returns a status 200(OK) from successful registration
 */

export const downloadReport = (vin,token) =>{
    
    return async dispatch =>{

      try{
         
            
          
       let response = await fetch('https://api.carfacts.ng/api/vin/'+ vin,{
        method:'GET',
        headers:{
          'content-type': 'application/json',
          'Authorization': 'Bearer '+ token
        },
      });
    
      let json = await response.json();
        const endpoint = json.file    
      window.open(endpoint,json.data.vin);
      

    }catch(error){

    }
}}

          

  