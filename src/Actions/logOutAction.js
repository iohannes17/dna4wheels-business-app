import {LOGIN_REQUEST,LOGOUT_SUCCESS,LOGOUT_ERROR} from '../Constants/Constants';





/**
 * @param {JSON} json
 * initiates when user loout successful
 */

export const UserlogoutSuccess = json=> ({
     type: LOGOUT_SUCCESS,
     payload:json,
});


/**
 * Initiates when user login failed
 * @param {String} error 
 */
export const Userlogouterror = (error) => ({
  type: LOGOUT_ERROR,
  payload: error,
});




/**
 * 
 * @param {JSON} userData 
 * @function UserLogin
 * Initates user login
 */


export const UserLogout = () => dispatch =>{

  try{
    dispatch(UserlogoutSuccess(''));
   
  }catch(e){
    dispatch(Userlogouterror(''));
   
  }
   
   
  } 

