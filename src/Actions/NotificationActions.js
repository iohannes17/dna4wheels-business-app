import {NOTIFICATION_SUCCESS,NOTIFICATION_ERROR,NOTIFICATION_REQUEST} from '../Constants/Constants';



/**
 * 
 * @param {array} json 
 * Initiates notification request
 */
export const NotificationRequest = ()=>({type: NOTIFICATION_REQUEST});

/**
 * @param{string} error
 * throws error in notification request
 */

export const NotificationError = error => ({
  type: NOTIFICATION_ERROR,
  payload: error,
});



/**
 * @param {array} data
 * returns array of objects
 */

export const NotificationSuccess = data => ({
    type: NOTIFICATION_SUCCESS,
    payload: data,
  });
  




export const NotificationHistory = (token) =>{
    
   return async dispatch =>{
     try{

       dispatch(NotificationRequest());
     
       
    let response = await fetch('https://api.carfacts.ng/api/notifications',{
      method:'GET',
      headers:{
      'content-type': 'application/xml',
      'Authorization': 'Bearer '+ token
      },
    
  });
        let json = await response.json();
          console.log(json)
        
       dispatch(NotificationSuccess(json.data));
      
     

     }catch(error){
        dispatch(NotificationError(error));
    
     }
   
   };
  } ;



