/**
 * @export ChangePassword
 * 
 */

import {CHANGE_PASSSWORD_SUCCESS,CHANGE_PASSWORD_ERROR,CHANGE_PASSWORD_REQUEST} from '../Constants/Constants';




/**
 * Called when user initiates a password chnage request
 */
export const changePassRequest = () =>({
    type: CHANGE_PASSWORD_REQUEST,
    payload:'',
});


/**
 * Called when registration is successful
 * @param {String} json
 */

export const changePassSuccess = json=> ({
     type:CHANGE_PASSSWORD_SUCCESS,
     payload:json,
});


/**
 * Called when password change fails
 * @param {String} error
 */

export const changePassError = (error) => ({
  type: CHANGE_PASSWORD_ERROR,
  payload: error,
});





/**
 * @function changePassword
 * @param {String} email
 * @return {String} response
 * returns a status 200(OK) from successful registration
 */

  export const changePassword = (email,token) => dispatch =>{

    dispatch(changePassRequest());
  
   fetch('https://api.carfacts.ng/api/change-password',{
     method:'PUT',
     headers:{
       'content-type': 'application/json',
       'Authorization': 'Bearer '+ token
     },
     body:JSON.stringify(email)
   })
   . then(res => {

       if(res.status === 202){
        dispatch(changePassSuccess(''))
         window.alert('Password Change Successful')    
       }

       else{
        window.alert('Password Change Error')    
       }
           
          
        })
        .catch((error) => {
                  dispatch(changePassError(error)); 
          });
      } 





          

  