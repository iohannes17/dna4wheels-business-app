/**
 * Get Report from user
 */

import {DOWNLOAD_ERROR,DOWNLOAD_REQUEST,DOWNLOAD_SUCCESS} from '../Constants/Constants';



const initialState ={
    isAvailable: false
  
};


const downloadReportReducer = (state = initialState, action) =>{

    switch(action.type){

        case DOWNLOAD_REQUEST:
             return{ ...state, isLoading:true};

         case DOWNLOAD_ERROR:
              return{ ...state, isLoading:false,isAvailable:action.payload};

         case DOWNLOAD_SUCCESS:
         
              return { ...state, isLoading:false, isAvailable:action.payload};
 
         default:
            return state;     
    }

};


export default downloadReportReducer;