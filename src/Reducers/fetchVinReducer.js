/**
 * Reducer updates state with Success{VIN/Chassis number basic details from API}
 * or Error message
 */

import {FETCH_VIN_ERROR,FETCH_VIN_SUCCESS} from '../Constants/Constants';



const initialState ={
   vinhistoryList:[],
   isAvailable: false,
   isFetching: true,
};


const fetchVinHistoryReducer = (state = initialState, action) =>{
   
 
    switch(action.type){


         case FETCH_VIN_ERROR:
              return{...state, vinhistoryList:action.payload, isAvailable: false,  isFetching: false};

         case FETCH_VIN_SUCCESS:
         
              return {...state,vinhistoryList:action.payload, isAvailable: true,  isFetching: false};


         default:
            return state;     
    }

};
 export default fetchVinHistoryReducer;