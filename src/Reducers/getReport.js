/**
 * Get Report from user
 */

import {REPORT_ERROR,REPORT_REQUEST,REPORT_SUCCESS} from '../Constants/Constants';



const initialState ={
   
  isLoading:false
};


const getReportReducer = (state = initialState, action) =>{

    switch(action.type){

        case REPORT_REQUEST:
             return{ ...state, isLoading:true};

         case REPORT_ERROR:
              return{ ...state, isLoading:false};

         case REPORT_SUCCESS:
         
              return { ...state, isLoading:false};
 
         default:
            return state;     
    }

};


export default getReportReducer;