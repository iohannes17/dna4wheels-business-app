/**
 * Reducer updates and handles the login state of the user
 */

import {LOGOUT_ERROR,LOGOUT_SUCCESS} from '../Constants/Constants';



const initialState ={
    isLoggedIn: false,
    errorMessage:'',
    token:'',
    isFetching:false,
    isCorrect:''
};


const logoutReducer = (state = initialState, action) =>{

    switch(action.type){

        case LOGOUT_SUCCESS:
             return{...state, isLoggedIn:false, token:''};
        case LOGOUT_ERROR:
        return{...state, isLoggedIn:false, token:''};
        
     
 
         default:
            return state;     
    }
     
};


export default logoutReducer;