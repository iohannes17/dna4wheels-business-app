/**
 * Reducer updates and handles the login state of the user
 */

import {LOGIN_REQUEST,LOGIN_SUCCESS,LOGIN_ERROR} from '../Constants/Constants';



const initialState ={
    isLoggedIn: false,
    errorMessage:'',
    token:'',
    isFetching:false,
    isCorrect:''
};


const loginReducer = (state = initialState, action) =>{

    switch(action.type){

        case LOGIN_REQUEST:
             return{ ...state, isFetching:true};

         case LOGIN_ERROR:
              return{ ...state, isLoggedIn:false,isFetching:false,errorMessage:action.payload,isCorrect:'wrong'};

         case LOGIN_SUCCESS:
         
              return { ...state, isLoggedIn:true,isFetching:false,token:action.payload,isCorrect:''};

        

 
         default:
            return state;     
    }

};


export default loginReducer;