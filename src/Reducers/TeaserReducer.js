
/**
 * @exports fetchVinTeaserReducer 
 * Teaser Details Reducer
 * Updates states in store
 */


import {TEASER_ERROR,TEASER_SUCCESS,TEASER_REQUEST} from '../../src/Constants/Constants';

const initialState ={
   teaserList:'',
   isAvailable: false,
   isFetching: false,
   teaserRequest:false,
   teaserListTwo:[]
};


const fetchVinTeaserReducer = (state = initialState, action) =>{
    
 
    switch(action.type){


         case TEASER_ERROR:
              return{...state, teaserList:action.payload, isAvailable: false,  isFetching:false,teaserRequest:false};

         case TEASER_SUCCESS:
         
              return {...state,teaserList:action.payload, isAvailable: true,  isFetching: false,teaserRequest:false};
          
        case TEASER_REQUEST:
              return{ ...state, teaserRequest:true};

         default:
            return state;     
    }

};
 export default fetchVinTeaserReducer;