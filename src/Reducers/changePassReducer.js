/**
 * The reducer updates the state of user registration, password chnage
 */

import {CHANGE_PASSSWORD_SUCCESS,CHANGE_PASSWORD_ERROR,CHANGE_PASSWORD_REQUEST} from '../Constants/Constants';



const initialState ={
   
   isLoading:false,
   response:''
};


const changePassReducer = (state = initialState, action) =>{

    switch(action.type){

        case CHANGE_PASSWORD_REQUEST:
             return{ ...state, isLoading:true};

         case CHANGE_PASSWORD_ERROR:
              return{ ...state, isLoading:false};

         case CHANGE_PASSSWORD_SUCCESS:
         
              return { ...state, isLoading:false};
 
         default:
            return state;     
    }

};


export default changePassReducer;